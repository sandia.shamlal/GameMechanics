﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Health : MonoBehaviour {

    public float MaxHealth = 100;
    public float CurrentHealth = 0;
    public RectTransform HealthBar;
    // Use this for initialization
    void Start() {

        CurrentHealth = MaxHealth;
    }

    // Update is called once per frame
    void Update() {


    }


    public void GettingHit(int damage) {

        CurrentHealth -= damage;
        if (CurrentHealth <= 0) {

            CurrentHealth = 0;
          
        }

        if (CurrentHealth >= 100) {

            CurrentHealth = MaxHealth;

        }

        HealthBar.transform.localScale = new Vector3((CurrentHealth / MaxHealth), 1, 1);

        //HealthBar.sizeDelta = new Vector2(CurrentHealth, HealthBar.sizeDelta.y);
    }
}

