﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spikes : MonoBehaviour {

    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update() {

    }

    private void OnCollisionEnter2D(Collision2D collision) {

        var GotHit = collision.gameObject;
        var PlayerHealth = GotHit.GetComponent<Health>();
        if (PlayerHealth != null) {


            PlayerHealth.GettingHit(10);
        }

        // Destroy(gameObject);
    }
}
