﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    private Rigidbody2D PlayerRigidbody;
    public float PlayerSpeed;
    public bool FacingRight;
    public bool NormalGravity;

   
    void Start() {

        PlayerRigidbody = GetComponent<Rigidbody2D>();
        FacingRight = true;
        NormalGravity = true;
       

    }

    // Update is called once per frame
    void FixedUpdate() {
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");

        PlayerMovement(horizontal);
        Flip(horizontal);
        Grounded(vertical);



    }

    public void PlayerMovement(float horizontal) {
        PlayerRigidbody.velocity = new Vector2(horizontal * PlayerSpeed, PlayerRigidbody.velocity.y);
    }


    public void Flip(float horizontal) {
        if (horizontal > 0 && !FacingRight || horizontal < 0 && FacingRight) {
            FacingRight = !FacingRight;

            Vector3 XScale = transform.localScale;
            XScale.x *= -1;
            transform.localScale = XScale;

        }
    }
    void Grounded(float vertical) {

        if (vertical > 0 && NormalGravity || vertical < 0 && !NormalGravity) {
            NormalGravity = !NormalGravity;
            Vector3 YScale = transform.localScale;
            YScale.y *= -1;
            PlayerRigidbody.gravityScale *= -1;

            transform.localScale = YScale;
        }

    }

}




