﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour {

    public int timer = 100;
    public Text timerText;

	// Use this for initialization
	void Start () {

        StartCoroutine("CountDownTimer");
		
	}
	
	// Update is called once per frame
	void Update () {

        timerText.text = ("Time: " + timer);

        if (timer <= 0) {
            StopCoroutine("CountDownTimer");
            timerText.text = "Times up!";
        }
	}

    IEnumerator CountDownTimer() {

        while (true) {

            yield return new WaitForSeconds(1);
            timer--;
        }

    }
}
