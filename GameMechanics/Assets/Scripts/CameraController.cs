﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

    public GameObject player;
    public float cameraOffset;

    private Vector3 playerPosition;
    public float moveSpeed;
  

    // Use this for initialization
    void Start() {

       // cameraOffset = new Vector3 (transform.position.x - player.transform.position.x,transform.position.y,transform.position.z);

  
    }

    // Update is called once per frame
    void LateUpdate() {

        //transform.position = player.transform.position + cameraOffset;
        playerPosition = new Vector3(player.transform.position.x, transform.position.y, transform.position.z);

        if (player.transform.localScale.x > 0.0f) {
            playerPosition = new Vector3(playerPosition.x + cameraOffset, playerPosition.y, playerPosition.z);
        } else {
            playerPosition = new Vector3(playerPosition.x - cameraOffset, playerPosition.y, playerPosition.z);

        }

        transform.position = Vector3.Lerp(transform.position, playerPosition, moveSpeed * Time.deltaTime);
    }
}
