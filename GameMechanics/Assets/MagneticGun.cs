﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MagneticGun : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKey(KeyCode.Space))
        Shoot(true);
	}

    void Shoot(bool isPlus) {
        Vector3 mousePos = Input.mousePosition;
        mousePos = Camera.main.ScreenToWorldPoint(mousePos);

        RaycastHit2D hit = Physics2D.Raycast(transform.position, new Vector2(transform.localScale.x,0));

        Debug.DrawRay(transform.position, new Vector2(transform.localScale.x, 0), Color.red);
        if (hit.collider.gameObject.tag == "Plus") {
            hit.collider.transform.Translate(new Vector2(transform.localScale.x,0) * Time.deltaTime);
       
        }

    }
}
